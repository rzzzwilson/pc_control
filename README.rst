What is this?
=============

The *PC_Control* suite shows how to run a python program on your PC to
control hardware on the Arduino Uno.

The software is in two parts:

* A sketch that receives commands from the USB serial line and performs the desired action
* A control program written in python that runs on the PC and sends commands over the USB serial line

Arduino sketch
--------------

The code running on the Uno is in the *sketch* subdirectory.  The expected
circuit layout is here:

.. image:: PC_Control_Schematic.png

This is my Uno running the sketch.  The LED 1 is turned on:

.. image:: Uno_LED.jpg

This sketch can be used with the Arduino IDE to control the code on the Uno
using the Serial Monitor.  Start the IDE Serial Monitor and type commands into
the monitor, then press ENTER.

You can also use a program running on the PC to pass commands to the Uno.  See
the python code below.

Python code
-----------

A python program that can be used instead of the Arduino IDE  to control
the Uno is in the *python* subdirectory.
