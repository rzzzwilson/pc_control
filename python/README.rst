This directory holds the python code to control hardware on the Uno.

Requirements
------------

The *pyusb* module needs to be installed::

    pip install pyusb

Usage
-----

When run the code will look for an attached Uno device.  You can then start
typing commands and the sketch on the Uno will respond, like this::

    $ python pc_control.py
    Found one Uno device.
    > H;
    --------------Valid Commands--------------------
    H;       send help text to PC
    L0ON;    turn LED 0 on
    L0OFF;   turn LED 0 off
    L1ON;    turn LED 1 on
    L1OFF;   turn LED 1 off
    S;       send LED state to PC
    ------------------------------------------------
    > L1ON;S;
    LED 0 is OFF, LED 1 is ON

The python program just shows how to communicate with the code on the Uno.
There's no reason why you couldn't add more hardware to the Uno and have 
the python code accept a high-level command from the user to "move a robot arm",
for example, and the python code could break that down to a series of low-level
robot commands to do what the user wants.
