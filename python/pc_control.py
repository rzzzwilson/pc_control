#!/usr/bin/env python

"""
A program to communicate with and control a sketch running on a Unoi.
"""

import os
import sys
import time
import serial
import usb
from serial.tools.list_ports import comports


###############################################################################
# A class to interface with an Uno's serial device.
#
#   # create object, optional baudrate, ignore named devices
#   dev = SerialDevice(description, ignore=None, baudrate=115200)
#
#   # execute command, return results
#   response = dev.do_cmd('H;')
#
#   # close device
#   dev.close()
###############################################################################

class SerialDeviceError(Exception):
    """Exception raised if no SerialDevice found."""

    pass

class SerialDevice:

    # USB/FTDI board identifiers for an AtMega328 in an Arduino Uno
    # change these to match your particular board if necessary
    VendorID = 0x2341
    ProductID = 0x0043

    def __init__(self, ignore=None, baudrate=115200):
        """Connect to a single SerialDevice device.

        ignore       list of devices to ignore
        baudrate     speed of serial communication
        """

        # prepare internal state
        self.ser = None
        self.baudrate = baudrate
        self.ignore = []
        if ignore:
            self.ignore = ignore

        # get *all* SerialDevice devices on the USB bus
        result = []
        for usb_dev in serial.tools.list_ports.comports():
            if (usb_dev.vid == SerialDevice.VendorID
                    and usb_dev.pid == SerialDevice.ProductID):
                if usb_dev.device not in self.ignore:
                    result.append(usb_dev)

        if len(result) < 1:
            print("Reminder: the Arduino IDE must *NOT* be active!")
            raise SerialDeviceError('No SerialDevices found.')

        if len(result) > 1:
            raise SerialDeviceError('Too many SerialDevices found.')

        self.device = result[0]                     # save possible SerialDevice

        # open a serial connection to the possible SerialDevice
        self.port = self.device.device
        self.ser = serial.Serial(port=self.port, baudrate=baudrate)
        self.ser.timeout = 0.05                     # nice short timeout

    def _readchar(self):
        """Read one character from the serial device.

        Retry on SerialException.
        Returns an empty string if no character read.
        """

        for _ in range(10):
            try:
                return str(bytes(self.ser.read()), encoding='utf-8')
            except (serial.serialutil.SerialException, OSError):
                time.sleep(0.5)

        raise SerialDeviceError('SerialDevice device disconnected') from None

    def _read_line(self):
        """Read one line from the 'self.ser' connection."""
    
        line = ''
        count = 0
        while count < 3:
            char = self._readchar()
            if char:
                if char == '\n':
                    return line
                elif char != '\r':
                    line += char
                    count = 0
            else:
                count += 1
    
        return ''

    def _flush_input(self):
        """Throwaway all input from the device until a timeout."""

        count = 0
        while count < 3:
            line = self._read_line()
            if line:
                count = 0
            else:
                count += 1

    def do_cmd(self, cmd):
        """Send command to SerialDevice, return response."""

        # ignore empty commands
        if not cmd:
            return

        if cmd[-1] != '\n':
            cmd += '\n'
        try:
            self.ser.write(bytes(cmd, encoding='utf-8'))
        except (serial.serialutil.SerialException, OSError):
            raise SerialDeviceError('SerialDevice device disconnected') from None

        # read response
        response = []
        count = 0
        while count < 5:
            line = self._read_line()
            if line:
                line = line.strip()
                response.append(line)
                count = 0
            else:
                count += 1

        response = '\n'.join(response)
        return response

    def close(self):
        """Close the SerialDevice."""

        if self.ser is not None:
            self.ser.close()

    def __del__(self):
        """Delete the SerialDevice device."""

        self.close()

    def __str__(self):
        return f'SerialDevice() (baudrate={self.baudrate})'

#################################################################################

try:
    dp = SerialDevice()
except SerialDeviceError:
    print('No Uno device found')
    sys.exit()

# now take commands from the user and pass to the Uno
print("Found one Uno device.")
while True:
    try:
        cmd = input("> ")
    except (EOFError, KeyboardInterrupt):
        print()
        break

    result = dp.do_cmd(cmd)
    if result is None:
        print("Uno was disconnected.")
        break

    print(result)
