//////////////////////////////////////////////////////////////////
// PC_Control Client
//
// Code that runs on an Uno and performs commands from the PC serial
// connection.  Commands are:
//
//    H;            prints help
//    L0ON;         turns LED 0 on
//    L0OFF;        turns LED 0 off
//    L1ON;         turns LED 1 on
//    L1OFF;        turns LED 1 off
//    S;            print the state of the LEDs
//////////////////////////////////////////////////////////////////

// program name and version
const char *ProgramName = "Uno Control";
const char *Version = "0.1";

// define pins for the two LEDs
// connect LEDs between pin and GND, including resistor
const int Led0Pin = 7;
const int Led1Pin = 6;

// length of buffer to gather external command strings
// "end of command" delimiter is ';'
const int MaxCmdLen = 32;
const char CmdEndChar = ';';

char CommandBuffer[MaxCmdLen + 1];  // holds complete commands, plus '\0' delimiter
int CommandIndex = 0;               // index of place to add new char in buffer
bool overflow = false;              // "true" if command buffer overflowed

// variables to hold the state of the two LEDs
bool Led0On = false;
bool Led1On = false;

// error message
const char *ErrorMsg = "ERROR";

// macro for comparing strings more naturally
#define STREQ(a, b) (strcmp((a), (b)) == 0)

// macro to turn off warnings about "unused parameter" in functions
#define UNUSED(v)   (void) (v)


/////////////////////////////////////////////////////////////
// Convert a string to uppercase in place.
/////////////////////////////////////////////////////////////

void str2upper(char *str)
{
  while (*str)
  {
    *str = toupper(*str);
    ++str;
  }
}

//##############################################################################
// External command routines.
//
// External commands are:
//    H;            sends help text to PC
//    L0ON;         turns LED 0 on
//    L0OFF;        turns LED 0 off
//    L1ON;         turns LED 1 on
//    L1OFF;        turns LED 1 off
//    S;            send LED state to PC
//##############################################################################

//----------------------------------------
// Get help:
//     H
//----------------------------------------

void xcmd_help(char *cmd)
{
  // turn off warning about "unused parameter"
  UNUSED(cmd);

  Serial.println("--------------Valid Commands--------------------");
  Serial.println("H;       send help text to PC");
  Serial.println("L0ON;    turn LED 0 on");
  Serial.println("L0OFF;   turn LED 0 off");
  Serial.println("L1ON;    turn LED 1 on");
  Serial.println("L1OFF;   turn LED 1 off");
  Serial.println("S;       send LED state to PC");
  Serial.println("------------------------------------------------");
}

//----------------------------------------
// Turn LED on/off
//     Ln[ON|OFF]
//----------------------------------------

void xcmd_led(char *cmd)
{
  // first char is 'L', second character is LED, '0' or '1'
  char led_num = cmd[1];

  if (led_num != '0' && led_num != '1')
  {
    Serial.println(ErrorMsg);
    return;
  }

  // rest of command must be "ON" or "OFF"
  if (!STREQ(cmd+2, "ON") && !STREQ(cmd+2, "OFF"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // do the appropriate command
  if (led_num == '0')
  {
    // set LED 0 as required
    if (STREQ(cmd+2, "ON"))
    {
      digitalWrite(Led0Pin, HIGH);
      Led0On = true;
    }
    else
    {
      digitalWrite(Led0Pin, LOW);
      Led0On = false;
    }
  }
  else
  {
    // set LED 1 as required
    if (STREQ(cmd+2, "ON"))
    {
      digitalWrite(Led1Pin, HIGH);
      Led1On = true;
    }
    else
    {
      digitalWrite(Led1Pin, LOW);
      Led1On = false;
    }
  }
}

//----------------------------------------
// Print LED state
//     S
//----------------------------------------

void xcmd_state(char *cmd)
{
  // if not legal, complain
  if (!STREQ(cmd, "S"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // send state string back to PC
  Serial.print("LED 0 is ");
  Serial.print(Led0On ? "ON" : "OFF");
  Serial.print(", LED 1 is ");
  Serial.println(Led1On ? "ON" : "OFF");
}

//----------------------------------------
// Process an external command.
//     cmd     address of command string buffer
//
// 'cmd' is '\0' terminated with final ';' removed.
//----------------------------------------
void do_xcmd(char *cmd)
{
  // ensure the command string is uppercase
  // makes later handling simpler
  str2upper(cmd);

  // call command handler based on first char
  switch (cmd[0])
  {
    case 'H':   // show help test
      xcmd_help(cmd);
      return;
    case 'L':   // turn LED on/off
      xcmd_led(cmd);
      return;
    case 'S':   // show LED state
      xcmd_state(cmd);
      return;
  }

  // if we get here command is unrecognized
  Serial.println(ErrorMsg);
}

//----------------------------------------
// Gather single characters from Serial into the
// command buffer.  Execute each complete command.
//----------------------------------------

void do_external_commands(void)
{
  // gather characters from the serial connection, if any
  while (Serial.available())
  {
    char ch = Serial.read();

    // ignore newlines
    if (ch == '\n')
    {
      continue;
    }

    if (CommandIndex < MaxCmdLen)
    {
      // only add to buffer if command not over-length
      CommandBuffer[CommandIndex++] = ch;
    }
    else
    {
      overflow = true;
    }

    // if end of command, execute it
    if (ch == CmdEndChar)
    {
      if (overflow)
      {
        Serial.println("TOO LONG");
      }
      else
      {
        CommandBuffer[CommandIndex-1] = '\0'; // remove final ';'
        do_xcmd(CommandBuffer);               // execute the command
      }

      // reset input state variables ready for next command
      CommandIndex = 0;
      overflow = false;
    }
  }
}

void setup(void)
{
  // set up the Serial comms
  Serial.begin(115200);

  // set pin modes for LEDs
  pinMode(Led0Pin, OUTPUT);
  pinMode(Led1Pin, OUTPUT);

  // test that the LEDs are connected properly
  // both should light up for half a second
  digitalWrite(Led0Pin, HIGH);
  digitalWrite(Led1Pin, HIGH);
  delay(500);
  digitalWrite(Led0Pin, LOW);   // leave both LEDs off
  digitalWrite(Led1Pin, LOW);

  // set LED state variables
  Led0On = false;
  Led1On = false;
}

void loop()
{
  do_external_commands();    // do any external commands
}